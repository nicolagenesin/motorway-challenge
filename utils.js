const https = require('https')

const retry = (func, arg, delay = 1000) => new Promise(resolve => {
    func(arg)
        .then(resolve)
        .catch(() => {
            setTimeout(() => {
                console.log('Retrying...')
                retry(func, arg, delay).then(resolve)
            }, delay)
        })
})

const fetch = (path, resolve, reject, pathsExecuted) => {
    const hostname = 'motorway-challenge-api.herokuapp.com'
    const options = {
        hostname,
        port: 443,
        path,
        method: 'GET'
    }

    const req = https.request(options, res => {
        res.on('data', data => {
            const json = JSON.parse(data)

            pathsExecuted[path] = json
            resolve(json)
        })
    })

    req.on('error', () => {
        reject()
    })

    req.end()
}

const updateUsersMap = (usersMap, data) => {
    data.forEach(x => {
        if (isToday(x.date) || isWeekend(x.date)) {
            return
        }

        const currentUser = usersMap[x.id]

        if (!currentUser) {
            usersMap[x.id] = {
                id: x.id,
                name: x.name,
                numberOfVisits: 1,
            }
        }
        else {
            currentUser.numberOfVisits = currentUser.numberOfVisits + 1
        }
    }
    )
}

const getNumberOfPages = numberOfItems => {
    const itemsPerPage = 15 // Assumption.
    let numberOfPages = 0

    if (numberOfItems >= itemsPerPage) {
        numberOfPages = Math.ceil(numberOfItems / itemsPerPage)
    }
    else if (numberOfItems > 0) {
        numberOfPages = 1
    }

    return numberOfPages
}

const isToday = dateString => {
    const date = new Date(dateString)
    const today = new Date()
    return date.getDate() == today.getDate() &&
        date.getMonth() == today.getMonth() &&
        date.getFullYear() == today.getFullYear()
}

const isWeekend = dateString => {
    const date = new Date(dateString)
    const day = date.getDay()
    return (day === 6) || (day === 0)
}

const createPaths = (arraySize, offset, token) => {
    return [...Array(arraySize).keys()].map(pageNumber => {
        return `/api/visits?page=${pageNumber + offset}&token=${token}`
    })
}

const logUsersToTable = usersMap => {
    const users = Object
        .keys(usersMap)
        .map(key => usersMap[key])

    console.table(users)
}

module.exports = {
    retry,
    fetch,
    updateUsersMap,
    getNumberOfPages,
    createPaths,
    logUsersToTable,
}
