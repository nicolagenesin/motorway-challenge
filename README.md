## Considerations

Since there were no requirements, I decided to get back to Node after more than a year without it! I went for a minimal implementation by requiring the https module only so it could run without the installation of external packages.

## Concept

The script will:

1) Fetch the token

2) Fetch the first page to acknowledge the total number of items

3) Fetch the next pages in parallel (only if the number of pages is > 1). If the total number of items has changed in any of the responses, the script could require new Promises to be created (a new user "Adam" could shift the whole set, potentially causing an element to fall on a new page). Only promises that were not resolved will result in a new api call being executed. I've added a very simple retry mechanism at the Promise level (see the retry() method).

4) After the recursive Promise.all(), the results will be processed and displayed in a table.

## Run

    git clone https://bitbucket.org/nicolagenesin/motorway-challenge
    node run
