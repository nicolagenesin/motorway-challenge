const utils = require('./utils')

const fetchToken = () => {
    return new Promise((resolve, reject) => {
        utils.fetch('/api/login', resolve, reject, pathsExecuted)
    })
}

// Object used to keep track of what Promises have been successfully resolved.
const pathsExecuted = {}
const fetchPage = path => {
    return new Promise((resolve, reject) => {
        if (pathsExecuted[path]) {
            // This promise has been already resolved in the past, so resolve with the previous result.
            console.log('Promise for path', path, 'created. This will be discarded upon execution because it has been resolved already')
            resolve(pathsExecuted[path])
        }
        else {
            console.log('Promise for path', path, 'created.')
            utils.fetch(path, resolve, reject, pathsExecuted)
        }
    })
}

const recursivePromiseAll = (pagesInformation, array, token) => {
    console.log('# of promises that will be evaluated by this Promise.all() (including the ones that can potentially be skipped)', array.length)

    return Promise.all(array).then((pageResponse) => {
        let otherPages = []

        pageResponse.forEach(result => {
            // Check if the dataset has changed. If that's the case,
            // append new urls so we can check them through new processes
            // Why? A new user "Adam" could shift the whole set,
            // potentially causing an element to fall on a new page.
            if (result.total > pagesInformation.numberOfItems) {
                const newNumberOfPages = utils.getNumberOfPages(result.total)

                if (newNumberOfPages > pagesInformation.numberOfPages) {
                    pagesInformation.numberOfPages = newNumberOfPages
                    pagesInformation.numberOfItems = result.total

                    console.log('[Warning] # of items changed to', pagesInformation.numberOfItems)
                    console.log('[Warning] # of pages changed to', pagesInformation.numberOfPages)

                    const paths = utils.createPaths(pagesInformation.numberOfPages - 1, 2, token)
                    otherPages = paths.map(path => utils.retry(fetchPage, path))
                }
            }
        })

        if (!otherPages.length) {
            console.log('No other page has to be fetched.')
            return pageResponse
        }

        console.log('One or more pages have been added. Performing another Promise.all() discardind the resolved promises right away.')

        return recursivePromiseAll(pagesInformation, otherPages)
    })
}

const init = async () => {
    const token = (await fetchToken()).token
    // We don't know what's the total number of items, so we'll have to perform a single
    // api call first to determine how many api calls we'll have to perform in parallel.
    const response = await fetchPage(`/api/visits?page=1&token=${token}`)
    const usersMap = {}
    const pagesInformation = {
        numberOfItems: response.total,
        numberOfPages: utils.getNumberOfPages(response.total)
    }

    utils.updateUsersMap(usersMap, response.data)

    if (pagesInformation.numberOfPages > 1) {
        const paths = utils.createPaths(pagesInformation.numberOfPages - 1, 2, token)
        const promises = paths.map(path => utils.retry(fetchPage, path))
        const results = await recursivePromiseAll(pagesInformation, promises, token)

        results.forEach(result => utils.updateUsersMap(usersMap, result.data))
        utils.logUsersToTable(usersMap)
    }
    else if (pagesInformation.numberOfPages === 1) {
        utils.logUsersToTable(usersMap)
    }
    else {
        console.log('There are no results.')
    }
}

init()
